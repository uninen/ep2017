# EuroPython 2017 Django + JavaScript Demo

This is a simple demo project of the EuroPython 2017 talk <a href="https://speakerdeck.com/uninen/pythonic-javascript-for-web-developers">Pythonic JavaScript for Web Developers (slides)</a> for demonstrating the use of modern JavaScript tools with Django. Feel free to ping me on Twitter -- <a href="https://twitter.com/uninen">@uninen</a> -- for any questions!

**Note:** The HTML/JS part of the code does not do anything atm. I'll push more example code and documentation during next few days.


## Demo Project


### Installing

The easiest way to get this project working is to use Docker. Dockerfile and docker-compose file is included.


### Running

Start Django runserver and then `yarn dev` to start WebPack development server. (Yarn is fast `npm` compatible package manager for installing npm packages and running node scripts.) This command is defined in `package.json` and uses configuration from `conf/webpack.dev.js`.


### Building static assets

Run `yarn build` to build static assets using `conf/webpack.prod.js` WebPAck configuration.


## Link list

Tools:

* <a href="https://www.npmjs.com/">NPM</a> package manager and registry
* <a href="https://yarnpkg.com/en/">Yarn</a> package manager
* <a href="https://babeljs.io/">Babel</a>, JavaScript compiler (transpiler)
* <a href="http://eslint.org/">ESLint</a>, pluggable linter for JavaScript
    * <a href="https://github.com/airbnb/javascript">AirBnb coding style</a> manual
* <a href="https://standardjs.com/">JavaScript Standard Style</a>
* <a href="https://webpack.js.org/">WebPack</a>, module builder that can build JavaScript, SASS, React and Vue components etc
* <a href="https://vuejs.org/">Vue.js</a>, easy to use and well documented progressive JavaScript framework
* <a href="https://devexpress.github.io/testcafe/">TestCafe</a>, is a modern end to end test tool for Web apps

other related links:

* <a href="https://www.w3.org/community/webed/wiki/A_Short_History_of_JavaScript">A Short History of JavaScript</a>
* <a href="https://benmccormick.org/2015/09/14/es5-es6-es2016-es-next-whats-going-on-with-javascript-versioning/">ES5, ES6, ES2016, ES.Next: What's going on with JavaScript versioning?</a>
* <a href="http://kangax.github.io/compat-table/es6/">ECMAScriptcompatibility table</a>
* <a href="https://github.com/lukehoban/es6features">Overview of ECMAScript 6 features</a>
* <a href="https://tidycms.com/">Tidy</a> is a fast and modern sitebuilder built with Vue


## Image Credits

* https://www.flickr.com/photos/humansquid/7049238949/
* https://www.flickr.com/photos/bedworth/8720079103/
* https://www.flickr.com/photos/pedrosimoes7/6524449233/
* https://www.flickr.com/photos/chadrogers24/5136270441/

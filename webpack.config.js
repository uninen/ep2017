function buildConfig(env) {
  return require('./conf/webpack.' + env + '.js')
}

module.exports = buildConfig

'use strict'

if (module.hot) {
  module.hot.accept()
}

import Vue from 'vue'

window.$ = require('jquery')
window.jQuery = $
window.Cookies = require('js-cookie')

Vue.config.productionTip = false

require('../css/epdemo.sass')


;(function () {

  let vueInstance = new Vue({
    el: '#maincontainer',

    data() {
      return {
        numItems: 1,
        itemPrice: 6
      }
    },

    computed: {
      totalSum() {
        return this.numItems * this.itemPrice
      },
    },

    methods: {

      apiPOST(url, payload) {
        return $.ajax({
          type: 'POST',
          url: '/api' + url,
          data: payload,
          headers: {'X-CSRFToken': Cookies.get('csrftoken')}
        })
      }

    },

    created() {
      console.log('Hello EuroPython 2017!')
    }
  })

}())

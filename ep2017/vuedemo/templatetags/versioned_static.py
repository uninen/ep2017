import subprocess

from django import template
from django.utils.safestring import mark_safe
from django.conf import settings

register = template.Library()

SHORT_GIT_HASH = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).strip()

if settings.DEBUG and settings.STATIC_DEBUG:
    filename_hash = 'dev'
    build_dir = ''
    server_string = settings.WEBPACK_SERVER_URL
else:
    filename_hash = SHORT_GIT_HASH
    build_dir = 'build/'
    server_string = ''


@register.simple_tag
def versioned_js(filename):
    """

    development:
    {% versioned_js "demo" %} -> localhost:3000/static/js/demo.dev.min.js

    production:
    {% versioned_js "demo" %} -> /static/js/build/demo.97e1fd8.min.js

    """

    base_path = '%s%sjs/%s' % (server_string, settings.STATIC_URL, build_dir)
    name = '%s.%s.min.js' % (filename, filename_hash)

    return mark_safe('<script src="%s%s"></script>' % (base_path, name))


@register.simple_tag
def versioned_css(filename):
    """

    development:
    {% versioned_css "demo" %} -> ''

    production:
    {% versioned_css "demo" %} -> <link href="/static/css/build/demo.97e1fd8.min.css" rel="stylesheet">

    """
    if settings.DEBUG and settings.STATIC_DEBUG:
        return ''
    else:
        base_path = '%s%scss/%s' % (server_string, build_dir, settings.STATIC_URL)
        name = '%s.%s.min.css' % (filename, filename_hash)

        return mark_safe('<link href="%s%s" rel="stylesheet">' % (base_path, name))

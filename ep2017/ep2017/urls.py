from django.conf.urls import url

from vuedemo.views import index

urlpatterns = [
    url(r'^$', index),
]

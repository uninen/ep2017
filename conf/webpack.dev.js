const common = require('./webpack.common')

const webpack = require('webpack')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')

const shortHash = 'dev'
const buildTime = Date.now()
const basePath = '/code'

module.exports = {
  entry: common.entryList,
  output: {
    path: basePath + '/webpack-assets/',
    filename: 'js/[name].' + shortHash + '.min.js',
    publicPath: 'http://127.0.0.1:3000/static/'
  },
  devServer: {
    port: 3000,
    host: '0.0.0.0',
    hot: true,
    hotOnly: true,
    inline: true,
    public: '127.0.0.1:3000',
    historyApiFallback: true,
    publicPath: 'http://127.0.0.13000/static/',
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
    }
  },
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js'
    }
  },
  devtool: 'cheap-eval-source-map',
  cache: true,
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [['es2015'], 'stage-2'],
          cacheDirectory: true,
        },
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
          },
          cssSourceMap: true,
        }
      },
      {
        test: /\.(sass|scss)$/,
        exclude: /node_modules/,
        use: [{loader: 'style-loader'}, {
          loader: 'css-loader',
          options: {
            sourceMap: true
          }
        }, {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }]
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new FriendlyErrorsWebpackPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"development"'
      },
      'DEBUG': true,
      '__ENV__GIT_HASH': JSON.stringify(shortHash),
      '__ENV__BUILD_DATE': JSON.stringify(buildTime),
    })
  ],
}

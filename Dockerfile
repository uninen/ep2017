FROM python:3.5
ENV PYTHONUNBUFFERED 1

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && curl -sL https://deb.nodesource.com/setup_7.x | bash - \
    && apt-get update && apt-get install yarn -y \
    && mkdir /code

COPY requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt

COPY package.json /code/package.json
RUN cd /code/ && yarn install

WORKDIR /code
COPY . /code/
